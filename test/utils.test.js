import { collectionDiff, configuredCollectionDiff } from '../src/utils';

describe('utils.collectionDiff', () => {
  it('should correctly list added, removed & updated', () => {
    expect.assertions(7);
    const prev = [{ id: '1', foo: 'bar' }, { id: '2' }, { id: '3', bar: 'foo' }, { id: '4' }];
    const next = [
      { id: '3', bar: 'new' },
      { id: '1', foo: 'bar' },
      { id: '4', baz: 123 },
      { id: '5' },
    ];
    const result = collectionDiff(prev)(next);
    expect(result.added[0].id).toBe('5');
    expect(result.added[1]).toBeUndefined();
    expect(result.removed[0].id).toBe('2');
    expect(result.removed[1]).toBeUndefined();
    expect(result.updated[0].id).toBe('3');
    expect(result.updated[1].id).toBe('4');
    expect(result.updated[2]).toBeUndefined();
  });

  it('should ignore ignoreKeys for updated determination', () => {
    expect.assertions(3);
    const prev = [{ id: '3', bar: 'foo' }];
    const next = [{ id: '3', bar: 'new' }];
    const result = configuredCollectionDiff({ ignoreKeys: ['bar'] })(prev)(next);
    expect(result.added[0]).toBeUndefined();
    expect(result.removed[0]).toBeUndefined();
    expect(result.updated[0]).toBeUndefined();
  });

  it('should skip if skipIf returns true', () => {
    expect.assertions(3);
    const prev = [{ id: '3', bar: 'foo' }];
    const next = [{ id: '3', bar: 'new' }];
    const result = configuredCollectionDiff({ skipIf: ({ bar }) => bar === 'new' })(prev)(next);
    expect(result.added[0]).toBeUndefined();
    expect(result.removed[0]).toBeUndefined();
    expect(result.updated[0]).toBeUndefined();
  });

  it('should transform each updated', () => {
    expect.assertions(5);
    const prev = [{ id: '3', bar: 'foo' }];
    const next = [{ id: '3', bar: 'new' }];
    const result = configuredCollectionDiff({
      transformUpdated: (_, __, patch) => patch,
    })(prev)(next);
    expect(result.added[0]).toBeUndefined();
    expect(result.removed[0]).toBeUndefined();
    expect(result.updated[0][0].op).toBe('replace');
    expect(result.updated[0][0].path).toBe('/bar');
    expect(result.updated[0][0].value).toBe('new');
  });
});
