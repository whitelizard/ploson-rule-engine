import * as R from 'ramda';

import { createRuleEngine } from '../src/manager';
import { createTimeout } from '../src/utils';

/* eslint-disable jest/no-done-callback, max-lines, max-lines, fp/no-mutation */

describe('add rule and trigger via update', () => {
  it('should add and start a rule actor that a trigger fires', (done) => {
    expect.assertions(3);
    const onLoadAssert = (val) => expect(val).toBe(1);
    const triggerActorAssert = (val) => expect(val).toBe(2);
    const actionAssert = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      ttl: undefined,
      onLoad: ['onLoadAssert', 1],
      action: [',', ['actionAssert', 3], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: ['=>', 'send', [',', ['triggerActorAssert', 2], ['$send']]],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { stop, onLoadAssert, triggerActorAssert, actionAssert },
      updater: (cbSend) => {
        cbSend({ type: 'ADD_RULE', data: rule });
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 0);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('update a rule, no hysteresis reset', () => {
  it('should not reset hysteresis (fired state) on other change than condition', (done) => {
    expect.assertions(3);
    const actionAssert = (val) => expect(val).toBe(1);
    const resetActionAssert = (val) => expect(val).toBe(2);
    const actionAssert2 = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 1],
      resetCondition: ['R.equals', '$payload', 5],
      resetAction: ['resetActionAssert', 1],
    };
    const newRule = {
      ...rule,
      action: [',', ['actionAssert2', 3], ['stop']],
      resetCondition: ['R.equals', '$payload', 8],
      resetAction: ['resetActionAssert', 2],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        [
          'R.pipe',
          ['createTimeout', '$send', 0, 7], // triggered
          ['createTimeout', '$send', 10, 8], // nothing
          ['createTimeout', '$send', 60, 5], // nothing
          ['createTimeout', '$send', 70, 8], // reset
          ['createTimeout', '$send', 80, 7], // trigger for done
        ],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, actionAssert2, resetActionAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_RULE', data: newRule }), 40);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('update a rule, hysteresis reset', () => {
  it('should reset hysteresis (fired state) on condition change', (done) => {
    expect.assertions(2);
    const actionAssert = (val) => expect(val).toBe(1);
    const resetActionAssert = (val) => expect(val).toBe(2);
    const actionAssert2 = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      ttl: undefined,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 1],
      resetCondition: ['R.equals', '$payload', 5],
      resetAction: ['resetActionAssert', 'NEVER'],
    };
    const newRule = {
      ...rule,
      action: [',', ['actionAssert2', 3], ['stop']],
      condition: ['R.equals', '$payload', 9],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        [
          'R.pipe',
          ['createTimeout', '$send', 0, 7], // triggered
          ['createTimeout', '$send', 60, 9], // trigger for done
        ],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, actionAssert2, resetActionAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_RULE', data: newRule }), 40);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
    setTimeout(() => service.stop(), 200);
  });
});

describe('update a rule, inactivate', () => {
  it('should inactivate rule if now active=false', (done) => {
    expect.assertions(2);
    // assertion should only be called once. Second time the rule should be inactive.
    const actionAssert = (val) => expect(val).toBe(1);
    const actionAssert2 = (val) => expect(val).toBe(2);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 1],
    };
    const newRule1 = {
      ...rule,
      active: false,
    };
    const newRule2 = {
      ...rule,
      active: true,
      action: [',', ['actionAssert2', 2], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        [
          'R.pipe',
          ['createTimeout', '$send', 0, 7],
          ['createTimeout', '$send', 40, 7],
          ['createTimeout', '$send', 70, 7],
        ],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, actionAssert2 },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_RULE', data: newRule1 }), 30);
        setTimeout(() => cbSend({ type: 'UPDATE_RULE', data: newRule2 }), 60);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('delete a rule', () => {
  it('should remove and inactivate', (done) => {
    expect.assertions(0);
    // assertion should never be called.
    const actionAssert = (val) => expect(val).toBe(44);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 1],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        ['R.pipe', ['createTimeout', '$send', 30, 7], ['createTimeout', 'stop', 70]],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'DELETE_RULE', data: { id: 'r1' } }), 20);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('update a trigger', () => {
  it('should inactivate if now active=false', (done) => {
    expect.assertions(2);
    // assertion should only be called once. Other times the trigger should be inactive.
    const actionAssert = (val) => expect(val).toBe(7);
    const triggerAssert = (val) => expect(val).toBe(2);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', '$payload'],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        ['R.pipe', ['createTimeout', '$send', 0, 7], ['createTimeout', '$send', 40, 7]],
      ],
    };
    const newTrigger1 = {
      ...trigger,
      active: false,
    };
    const newTrigger2 = {
      ...trigger,
      active: true,
      trigger: ['=>', 'send', [',', ['triggerAssert', 2], ['stop']]],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, triggerAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGER', data: newTrigger1 }), 30);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGER', data: newTrigger2 }), 60);
      },
    };
    service = createRuleEngine(engineConfig);
    // .onTransition(({ value }) => console.log(value));
    service.start();
  });
});

describe('delete a trigger', () => {
  it('should inactivate and remove', (done) => {
    expect.assertions(0);
    // assertion should never be called.
    const actionAssert = (val) => expect(val).toBe(44);
    const rule = {
      id: 'r1',
      active: true,
      ttl: undefined,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 1],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: ['=>', 'send', ['createTimeout', '$send', 30, 7]],
    };
    const trigger2 = {
      id: 't2',
      active: true,
      targets: ['r1'],
      trigger: ['=>', 'send', ['createTimeout', 'stop', 70]],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'ADD_RULE', data: rule }), 0);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger }), 10);
        setTimeout(() => cbSend({ type: 'ADD_TRIGGER', data: trigger2 }), 10);
        setTimeout(() => cbSend({ type: 'DELETE_TRIGGER', data: { id: 't1' } }), 20);
      },
    };
    service = createRuleEngine(engineConfig);
    // .onTransition(({ value }) => console.log(value));
    service.start();
  });
});
