import * as R from 'ramda';

import { createRuleEngine } from '../src/manager';
import { createTimeout } from '../src/utils';

/* eslint-disable jest/no-done-callback, max-lines, fp/no-mutation */

describe('engine init', () => {
  it('should start engine', (done) => {
    expect.assertions(0);
    const engineConfig = {};
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    service = createRuleEngine(engineConfig).onTransition(
      ({
        // value,
        matches,
      }) => {
        // console.log(value);
        if (matches({ running: 'idle' })) stop();
      },
    );
    service.start();
  });

  it('should ignore (only log) errors, e.g. when loading broken rule', (done) => {
    expect.assertions(0);
    const rule = {
      id: 'r1',
      active: 'foo',
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      ruleFetcher: async () => [rule],
    };
    service = createRuleEngine(engineConfig).onTransition(
      ({
        // value,
        matches,
      }) => {
        // console.log(value);
        if (matches({ running: 'idle' })) stop();
      },
    );
    service.start();
  });
});

describe('init rules & triggers via update', () => {
  it('should start a rule actor that a trigger fires', (done) => {
    expect.assertions(3);
    const onLoadAssert = (val) => expect(val).toBe(1);
    const triggerActorAssert = (val) => expect(val).toBe(2);
    const actionAssert = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      onLoad: ['onLoadAssert', 1],
      action: [',', ['actionAssert', 3], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: ['=>', 'send', [',', ['triggerActorAssert', 2], ['$send']]],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { stop, onLoadAssert, triggerActorAssert, actionAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [rule] }), 0);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [trigger] }), 10);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('rule reset', () => {
  it('should check resetCondition and run resetAction on second trigger', (done) => {
    expect.assertions(2);
    const actionAssert = (val) => expect(val).toBe(2);
    const actionAssert2 = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      ttl: undefined,
      resetCondition: ['R.equals', '$payload', 8],
      action: ['actionAssert', 2],
      // cooldown,
      condition: ['R.equals', '$payload', 7],
      resetAction: [',', ['actionAssert2', 3], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        ['R.pipe', ['createTimeout', '$send', 0, 7], ['createTimeout', '$send', 10, 8]],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      // ruleFetcher: async ,
      // triggerFetcher,
      staticEnv: { R, createTimeout, stop, actionAssert, actionAssert2 },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [rule] }), 0);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [trigger] }), 10);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('rule update', () => {
  it('should reset rule hysteresis (fired state) if it updates & condition changes', (done) => {
    expect.assertions(2);
    const actionAssert = (val) => expect(val).toBe(2);
    const resetActionAssert = (val) => expect(val).toBe('NEVER');
    const actionAssert2 = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 2],
      resetCondition: ['R.equals', '$payload', 8],
      resetAction: ['resetActionAssert', 0],
    };
    const newRule = {
      ...rule,
      condition: ['R.equals', '$payload', 8],
      action: [',', ['actionAssert2', 3], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        ['R.pipe', ['createTimeout', '$send', 0, 7], ['createTimeout', '$send', 40, 8]],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, resetActionAssert, actionAssert2 },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [rule] }), 0);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [trigger] }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [newRule] }), 30);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });

  it('should inactivate rule if updated with active=false', (done) => {
    expect.assertions(2);
    // assertion should only be called once. Second time the rule should be inactive.
    const actionAssert = (val) => expect(val).toBe(2);
    const actionAssert2 = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', 2],
    };
    const newRule1 = {
      ...rule,
      active: false,
    };
    const newRule2 = {
      ...rule,
      active: true,
      action: [',', ['actionAssert2', 3], ['stop']],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        [
          'R.pipe',
          ['createTimeout', '$send', 0, 7],
          ['createTimeout', '$send', 20, 7],
          ['createTimeout', '$send', 40, 7],
        ],
      ],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, actionAssert2 },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [rule] }), 0);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [trigger] }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [newRule1] }), 20);
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [newRule2] }), 40);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

describe('trigger update', () => {
  it('should inactivate trigger if updated with active=false', (done) => {
    expect.assertions(2);
    // assertion should only be called once. Other times the trigger should be inactive.
    const actionAssert = (val) => expect(val).toBe(7);
    const triggerAssert = (val) => expect(val).toBe(3);
    const rule = {
      id: 'r1',
      active: true,
      condition: ['R.equals', '$payload', 7],
      action: ['actionAssert', '$payload'],
    };
    const trigger = {
      id: 't1',
      active: true,
      targets: ['r1'],
      trigger: [
        '=>',
        'send',
        [
          'R.pipe',
          ['createTimeout', '$send', 0, 7], // 10
          ['createTimeout', '$send', 30, 7], // 40
          ['createTimeout', '$send', 40, 7], // 50
          ['createTimeout', '$send', 60, 7], // 70
        ],
      ],
    };
    const newTrigger1 = {
      ...trigger,
      active: false,
    };
    const newTrigger2 = {
      ...trigger,
      active: true,
      trigger: ['=>', 'send', [',', ['triggerAssert', 3], ['stop']]],
    };
    let service;
    const stop = () => {
      setTimeout(() => service.stop(), 0);
      done();
    };
    const engineConfig = {
      staticEnv: { R, createTimeout, stop, actionAssert, triggerAssert },
      updater: (cbSend) => {
        setTimeout(() => cbSend({ type: 'UPDATE_RULES', data: [rule] }), 0);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [trigger] }), 10);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [newTrigger1] }), 30);
        setTimeout(() => cbSend({ type: 'UPDATE_TRIGGERS', data: [newTrigger2] }), 60);
      },
    };
    service = createRuleEngine(engineConfig);
    service.start();
  });
});

// Xtest('should handle ttl expired', async (t) => {
// Xtest('should handle onLoad error', async (t) => {
// Xtest('should handle condition error', async (t) => {
// Xtest('should handle actions error', async (t) => {
// Xtest('should not run actions if conditions not met', async (t) => {
// Xtest('should handle cooldown - block', async (t) => {
// Xtest('should handle cooldown - pass on no cooldown', async (t) => {
// Xtest('should handle cooldown - pass on small cooldown and a wait', async (t) => {
// Xtest('should handle cooldown together with reset', async (t) => {
