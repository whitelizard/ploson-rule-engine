export const triggerCollection = [
  {
    id: 't1',
    active: true,
    trigger: [
      '=>',
      'send',
      [
        ',',
        ['console.log', '`Spawned callback t1 started`', '$send'],
        ['createInterval', '$send', 3_000, { bar: '`foo`' }],
        // ['cron', '$send', '`* * * * *`', { bar: '`foo`' }],
      ],
    ],
    targets: ['r1'],
  },
  // {
  //   id: 'assetTemps',
  //   assets: {
  //     '1:1': 1,
  //   },
  //   trigger:
  //     // ['mapFromDataset',
  //     [
  //       ',',
  //       ['subscribe', '`$asset.channels.humidity`', '<subArg1>'],
  //       ['cron', '`$asset.cron.checkStatus`', '<cronArg1>'],
  //     ],
  //   // ],
  //   triggerRaw: [
  //     '=>',
  //     'send',
  //     [
  //       ',',
  //       { dataset: ['getAssetDataset', { rids: ['of', '`1:1`'], projection: [] }] },
  //       [
  //         ',',
  //         ['mapFromAssets', 'subscribe', '`channels.humidity`', '$send', ['of', '<subArg1>']],
  //         ['mapFromAssets', 'cron', '`cron.checkStatus`', '$send', ['of', '<cronArg1>']],
  //         // -> {}
  //         // {channels: ['assetsChannelsOfId', 'vibration', ['assetsOfType', 'truck']]}
  //         // {channels: ['clientSubTo']}
  //         {
  //           // assetDataset: ['createDataset', { rids: [], filter: ['=', ['$prop', 'type'], ''] }],
  //           // assets: ['`asset1`', '`asset2`', '`asset3`'],
  //           // channelPath: ['=>', 'asset', ['R.join', '', ['of', '`channel/`', '$chRid']]],
  //           sub: [['R.pipe', 'R.curry', 'R.flip'], 'subscribe'],
  //         },
  //         ['R.map', ['R.pipe', '$channelPath', ['$sub', '$send']], '$assets'],
  //         // return unsubscribe function
  //       ],
  //     ],
  //   ],
  //   targets: ['tempWarn'],
  // },
];

export const ruleCollection = [
  {
    id: 'r1',
    active: true,
    ttl: Date.now() + 60_000,
    onLoad: [
      ',',
      { count: [['R.pipe', ['R.when', 'R.not', ['=>', 0]], 'R.inc'], '$count'] },
      ['console.log', '`Times loaded:`', '$count'],
    ],
    cooldown: 1_500,
    resetCondition: true,
    resetAction: [
      ',',
      { count: ['R.add', '$count', 1] },
      ['console.log', '`RULE-1 RESET with data:`', '$payload'],
    ],
    // condition: true,
    condition: [',', ['console.log', '$payload.bar'], ['R.equals', '`foo`', '$payload.bar']],
    // condition: [
    //   ',',
    //   { assets: ['$dataset.getRecords'] },
    //   { asset: '$assets.0' },
    //   ['R.lt', '$asset.thresholds.tempHigh', '$payload'],
    // ],
    action: [
      'console.log',
      '`RULE-1 triggered with data:`',
      ['R.add', '$count', ['R.length', '$payload.bar']],
    ],
  },
];
