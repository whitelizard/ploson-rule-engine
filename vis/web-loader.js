import { interpret } from 'xstate';
import { inspect } from '@xstate/inspect';
import { defaultEnv } from 'ploson';
import * as R from 'ramda';
import * as D from 'date-fns/fp';

// import { ruleMachine } from '../src/rule';
import { createRuleEngineMachine } from '../src';
import { createInterval, sleep } from '../src/utils';
import { ruleCollection, triggerCollection } from './mock';

// function* ruleMocks() {}
// const ruleMockIterator = ruleMocks();

// const ruleFetcher = async () => {
//   ruleMockIterator.next();
//   ruleCollection;
// };
const ruleFetcher = async () => ruleCollection;
const triggerFetcher = async () => triggerCollection;

const machine = createRuleEngineMachine({
  ruleFetcher,
  triggerFetcher,
  staticEnv: { ...defaultEnv, R, D, createInterval },
  // staticEnvTriggers: { ...defaultEnv, R, D, createInterval },
  updater: (cbSend) => {
    // console.log('UPDATER ACTOR STARTED');
    return createInterval(async () => {
      const data = await ruleFetcher();
      cbSend({ type: 'UPDATE_RULES', data });
      await sleep(500);
      const triggers = await triggerFetcher();
      cbSend({ type: 'UPDATE_TRIGGERS', data: triggers });
    }, 10000);
  },
});

// const tempExpiresIn = 20;
// const machine = ruleMachine.withContext({
//   // ttl: new Date(Date.now() + tempExpiresIn * 1000),
//   cooldown: 1500,
//   // hysteresis: true,
// });

inspect({
  // url: 'https://statecharts.io/inspect',
  // iframe: false,
  iframe: () => document.querySelector('iframe[data-xstate]'),
});

const service = interpret(machine, {
  devTools: true,
}).onTransition(() => {
  // console.log(state.value);
});

service.start();
// service.send('ACTIVATE');
