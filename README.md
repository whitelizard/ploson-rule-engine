# ploson-rule-engine

A real-time rule engine with [Ploson](https://www.npmjs.com/package/ploson) execution.

- Built with XState for high control and safety.
- Stays updated with any change to the rules.
  - Handles reload, restart, inactivation, de-triggering etc. automatically on updates.
- Perfect for listening on data messages on a network/bus and act on them.

"Rules" are just JSON, storable in a database, but they can contain custom code as complex as needed. You can process received data to make conditional decisions and aggregate/enhance/transform it to send it elsewhere.

An example of a rule:
```js
const rule = {
    id: 'r1',
    active: true,
    ttl: 17407501722694,
    onLoad: ['console.log', '`Rule loaded`'],
    cooldown: 1_500,
    resetCondition: true,
    resetAction: ['console.log', '`RULE-1 RESET with data:`', '$payload'],
    condition: ['R.equals', '`foo`', '$payload.bar'],
    action: [
      'console.log',
      '`RULE-1 triggered with data:`',
      ['R.add', '$count', ['R.length', '$payload.bar']],
    ],
  }
```

![Rule state machine in action](https://lh3.googleusercontent.com/u/0/drive-viewer/AFDK6gOEsUMLiHLB2aKj8azNCQBPgj-sDdI3mZgjfvxJiuFkuGJX7EW8wnRFGZnEUuFDArVdheQXY2A9pAEHE1eQ_fFYU_evnQ=w852-h914)

```mermaid
stateDiagram-v2
  state "rule" as rule
  state rule {
    state "inactive" as machine.inactive
    state "idle" as machine.idle
    state "checkExpired" as machine.checkExpired
    state "checkCondition" as machine.checkCondition
    state "executeAction" as machine.executeAction
    state "fired" as machine.fired
    state "expired" as machine.expired
    [*] --> machine.idle
    this(rule) --> machine.inactive: INACTIVATE
    
    machine.checkExpired --> machine.expired: *done
    machine.checkExpired --> machine.checkCondition: *error
    
    machine.checkCondition --> machine.executeAction: *done [conditionTruthy]
    machine.checkCondition --> machine.idle: *done [else]
    machine.checkCondition --> machine.idle: *error
    machine.fired --> machine.idle: *onDone

    machine.executeAction --> machine.fired: *done
    machine.executeAction --> machine.fired: *error
    
    state machine.fired {
      state "switcher" as machine.fired.switcher
      state "temperature" as machine.fired.temperature
      state machine.fired.switcher {
        state "decide" as machine.fired.switcher.decide
        state "flipped" as machine.fired.switcher.flipped
        state "checkCondition" as machine.fired.switcher.checkCondition
        state "executeResetAction" as machine.fired.switcher.executeResetAction
        state "done" as machine.fired.switcher.done
        [*] --> machine.fired.switcher.decide
        machine.fired.switcher.decide --> machine.fired.switcher.flipped: *always [hasHysteresis]
        machine.fired.switcher.decide --> machine.fired.switcher.done: *always [else]
        
        machine.fired.switcher.checkCondition --> machine.fired.switcher.executeResetAction: *done [conditionTruthy]
        machine.fired.switcher.checkCondition --> machine.fired.switcher.flipped: *done [else]
        machine.fired.switcher.checkCondition --> machine.fired.switcher.flipped: *error
        
        machine.fired.switcher.executeResetAction --> machine.fired.switcher.done: *done
        machine.fired.switcher.executeResetAction --> machine.fired.switcher.done: *error
        
        machine.fired.switcher.flipped --> machine.fired.switcher.checkCondition: FIRE
      }
      --
      state machine.fired.temperature {
        state "decide" as machine.fired.temperature.decide
        state "hot" as machine.fired.temperature.hot
        state "cool" as machine.fired.temperature.cool
        [*] --> machine.fired.temperature.decide
        machine.fired.temperature.decide --> machine.fired.temperature.hot: *always [hasCooldown]
        machine.fired.temperature.decide --> machine.fired.temperature.cool: *always [else]
        machine.fired.temperature.hot --> machine.fired.temperature.cool: *after [COOLDOWN]
      }
    }
    
    machine.inactive --> machine.idle: ACTIVATE
    machine.idle --> machine.checkExpired: FIRE
    
    machine.fired --> machine.idle: ABORT
  }
```

```mermaid
stateDiagram-v2
  state "rule" as rule
  state rule {
    state "inactive" as machine.inactive
    state "idle" as machine.idle
    state "checkCondition" as machine.checkCondition
    state "executeAction" as machine.executeAction
    state "fired" as machine.fired
    state "expired" as machine.expired
    [*] --> machine.idle
    state machine.fired {
      state "switcher" as machine.fired.switcher
      state "temperature" as machine.fired.temperature
      state machine.fired.switcher {
        state "decide" as machine.fired.switcher.decide
        state "flipped" as machine.fired.switcher.flipped
        state "checkCondition" as machine.fired.switcher.checkCondition
        state "executeResetAction" as machine.fired.switcher.executeResetAction
        state "done" as machine.fired.switcher.done
        [*] --> machine.fired.switcher.decide
        
        machine.fired.switcher.decide --> machine.fired.switcher.flipped: always [hasHysteresis]
        machine.fired.switcher.decide --> machine.fired.switcher.done: always [default]
        machine.fired.switcher.flipped --> machine.fired.switcher.checkCondition: FIRE 
        machine.fired.switcher.checkCondition --> machine.fired.switcher.executeResetAction: onDone [conditionTruthy]
        machine.fired.switcher.checkCondition --> machine.fired.switcher.flipped: onDone [default]
        machine.fired.switcher.checkCondition --> machine.fired.switcher.flipped: onError 
        machine.fired.switcher.executeResetAction --> machine.fired.switcher.done: onDone 
        machine.fired.switcher.executeResetAction --> machine.fired.switcher.done: onError
      }
      --
      state machine.fired.temperature {
        state "decide" as machine.fired.temperature.decide
        state "hot" as machine.fired.temperature.hot
        state "cool" as machine.fired.temperature.cool
        [*] --> machine.fired.temperature.decide
        
        machine.fired.temperature.decide --> machine.fired.temperature.hot: always [hasCooldown]
        machine.fired.temperature.decide --> machine.fired.temperature.cool: always [default]
        machine.fired.temperature.hot --> machine.fired.temperature.cool: COOLDOWN
      }
    }
    machine.inactive --> machine.idle: ACTIVATE 
    machine.idle --> machine.expired: FIRE [isExpired]
    
    machine.idle --> machine.checkCondition: PASS_ON 
    machine.fired --> machine.idle: ABORT 
    machine.checkCondition --> machine.executeAction: onDone [conditionTruthy]
    machine.checkCondition --> machine.idle: onDone [default]
    machine.checkCondition --> machine.idle: onError 
    machine.executeAction --> machine.fired: onDone 
    machine.executeAction --> machine.fired: onError
  }

```