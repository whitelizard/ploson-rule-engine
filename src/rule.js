import { createMachine, assign, send } from 'xstate';
import { ARR, OBJ } from 'ploson';
import { isBefore } from 'date-fns/fp';

import { runPlosonOrReject } from './processing';

const machineConf = {
  predictableActionArguments: true,
  id: 'rule',
  initial: 'idle',
  on: {
    INACTIVATE: 'inactive',
    UPDATE: { actions: 'updateRule' },
  },
  states: {
    inactive: { on: { ACTIVATE: 'idle' } },
    idle: {
      on: {
        FIRE: [{ cond: 'isExpired', target: 'expired' }, { actions: 'passOn' }],
        PASS_ON: 'checkCondition',
      },
    },
    checkCondition: {
      invoke: {
        src: 'condition',
        onDone: [{ cond: 'conditionTruthy', target: 'executeAction' }, { target: 'idle' }],
        onError: { actions: 'logError', target: 'idle' },
      },
    },
    executeAction: {
      invoke: {
        src: 'action',
        onDone: { target: 'fired' },
        onError: { actions: 'logError', target: 'fired' },
      },
    },
    fired: {
      type: 'parallel',
      on: { ABORT: 'idle' },
      states: {
        switcher: {
          initial: 'decide',
          states: {
            decide: {
              always: [{ cond: 'hasHysteresis', target: 'flipped' }, { target: 'done' }],
            },
            flipped: { on: { FIRE: 'checkCondition' } },
            checkCondition: {
              invoke: {
                src: 'resetCondition',
                onDone: [
                  { cond: 'conditionTruthy', target: 'executeResetAction' },
                  { target: 'flipped' },
                ],
                onError: { actions: 'logError', target: 'flipped' },
              },
            },
            executeResetAction: {
              invoke: {
                src: 'resetAction',
                onDone: { target: 'done' },
                onError: { actions: 'logError', target: 'done' },
              },
            },
            done: { type: 'final' },
          },
        },
        temperature: {
          initial: 'decide',
          states: {
            decide: {
              always: [{ cond: 'hasCooldown', target: 'hot' }, { target: 'cool' }],
            },
            hot: { after: { COOLDOWN: 'cool' } },
            cool: { type: 'final' },
          },
        },
      },
      onDone: 'idle',
    },
    expired: { type: 'final' },
  },
};

const options = {
  delays: {
    COOLDOWN: ({ cooldown }) => cooldown,
  },
  guards: {
    hasCooldown: ({ cooldown }) => Boolean(cooldown),
    hasHysteresis: ({ resetCondition }) => Boolean(resetCondition),
    conditionTruthy: (ctx, { data: [ok] = ARR }) => Boolean(ok),
    isExpired: ({ ttl }) => isBefore(new Date())(ttl),
  },
  services: {
    condition: ({ condition, ploson }, { data: { payload } }) => {
      // console.log('condition:', condition, '| payload:', payload);
      return runPlosonOrReject({
        ploson,
        payload,
        code: [',', { payload: Promise.resolve(payload) }, condition],
        createReturn: (res) => [res, payload],
      });
    },
    resetCondition: ({ resetCondition, ploson }, { payload }) =>
      runPlosonOrReject({
        ploson,
        payload,
        code: [',', { payload: Promise.resolve(payload) }, resetCondition],
        createReturn: (res) => [res, payload],
      }),
    action: async ({ action, ploson }, { data: [, payload] = ARR }) => {
      // console.log('action, payload:', payload);
      return runPlosonOrReject({
        ploson,
        payload,
        code: [',', { payload: Promise.resolve(payload) }, action],
      });
    },
    resetAction: ({ resetAction, ploson }, { data: [, payload] = ARR }) =>
      runPlosonOrReject({
        ploson,
        payload,
        code: [',', { payload: Promise.resolve(payload) }, resetAction],
      }),
  },
  actions: {
    passOn: send((ctx, { payload }) => ({ type: 'PASS_ON', data: { payload } })),
    logError: (ctx, { data }) => console.error(data),
    updateRule: assign({
      ttl: (ctx, { rule: { ttl } = OBJ }) => ttl,
      hysteresis: (ctx, { rule: { hysteresis } = OBJ }) => hysteresis,
      condition: (ctx, { rule: { condition } = OBJ }) => {
        // console.log('assign condition:', condition)
        return condition;
      },
      action: (ctx, { rule: { action } = OBJ }) => {
        // console.log('assign action:', action);
        return action;
      },
      cooldown: (ctx, { rule: { cooldown } = OBJ }) => cooldown,
      resetCondition: (ctx, { rule: { resetCondition } = OBJ }) => resetCondition,
      resetAction: (ctx, { rule: { resetAction } = OBJ }) => resetAction,
    }),
  },
};

export const ruleMachine = createMachine(machineConf, options);
