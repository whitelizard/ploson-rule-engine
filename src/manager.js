import { createMachine, assign, spawn, actions, send, interpret } from 'xstate';
import { createRunner, OBJ, ARR, FUNC } from 'ploson';
import * as R from 'ramda';

import { Rule } from './rule-dm';
import { configuredCollectionDiff } from './utils';
import {
  getAddedRule,
  getAddedTrigger,
  createPrepareAddedRule,
  createPrepareAddedTrigger,
  prepareUpdatedRule,
  prepareUpdatedTrigger,
  getUpdateRuleActions,
  updateTriggerAction,
  getBlindDatasetUpdateActions,
} from './manager-logic';

const { pure, stop } = actions;

const machineConf = {
  predictableActionArguments: true,
  id: 'rule-manager',
  initial: 'initRules',
  context: {
    $queue: [],
    rules: [
      // { ploson, ref, ...rule }
    ],
    triggers: [
      // { id, trigger, targets, ploson, callback, ref }
    ],
  },
  on: {
    ADD_RULE: { actions: 'defer' },
    ADD_TRIGGER: { actions: 'defer' },
    UPDATE_RULE: { actions: 'defer' },
    UPDATE_TRIGGER: { actions: 'defer' },
    DELETE_RULE: { actions: 'defer' },
    DELETE_TRIGGER: { actions: 'defer' },
  },
  states: {
    retryRules: { after: { RETRY_DELAY: { target: 'initRules' } } },
    initRules: {
      invoke: {
        src: 'ruleFetcher',
        onDone: { actions: 'blindRulesUpdate', target: 'initTriggers' },
        onError: { actions: 'logError', target: 'retryRules' },
      },
    },
    retryTriggers: { after: { RETRY_DELAY: { target: 'initTriggers' } } },
    initTriggers: {
      invoke: {
        src: 'triggerFetcher',
        onDone: { actions: 'blindTriggersUpdate', target: 'running' },
        onError: { actions: 'logError', target: 'retryTriggers' },
      },
    },
    running: {
      entry: 'spawnUpdater',
      initial: 'idle',
      on: {
        FIRE_RULES: { actions: 'fireRules' },
      },
      states: {
        idle: {
          entry: 'recallFromQueue',
          on: {
            UPDATE_RULES: { actions: 'blindRulesUpdate', target: 'idle' },
            UPDATE_TRIGGERS: { actions: 'blindTriggersUpdate', target: 'idle' },
            ADD_RULE: { target: 'prepareAddingRule' },
            ADD_TRIGGER: { target: 'prepareAddingTrigger' },
            UPDATE_RULE: { target: 'prepareUpdatingRule' },
            UPDATE_TRIGGER: { target: 'prepareUpdatingTrigger' },
            DELETE_RULE: { actions: 'removeRule' },
            DELETE_TRIGGER: { actions: 'removeTrigger' },
          },
        },
        prepareAddingRule: {
          invoke: {
            src: 'prepareAddedRule',
            onError: { actions: 'logError', target: 'idle' },
            onDone: { actions: 'addRuleExternal', target: 'idle' },
          },
        },
        prepareAddingTrigger: {
          invoke: {
            src: 'prepareAddedTrigger',
            onError: { actions: 'logError', target: 'idle' },
            onDone: { actions: 'addTriggerExternal', target: 'idle' },
          },
        },
        prepareUpdatingRule: {
          invoke: {
            src: 'prepareUpdatedRule',
            onError: { actions: 'logError', target: 'idle' },
            onDone: { actions: 'updateRuleExternal', target: 'idle' },
          },
        },
        prepareUpdatingTrigger: {
          invoke: {
            src: 'prepareUpdatedTrigger',
            onError: { actions: 'logError', target: 'idle' },
            onDone: { actions: 'updateTriggerExternal', target: 'idle' },
          },
        },
      },
    },
  },
};

const getOptions = ({
  ruleFetcher = () => Promise.resolve(ARR),
  triggerFetcher = () => Promise.resolve(ARR),
  staticEnv = OBJ,
  staticEnvTriggers = staticEnv,
  updater = FUNC,
}) => {
  const diff = {
    rules: configuredCollectionDiff({
      ignoreKeys: ['ref', 'ploson', 'active'],
      // skipIf: ({ active }) => !active,
      autoRemoveCondition: ({ active }) => !active,
      transformAdded: (item) => {
        const { value, error, warning } = Rule.validate(item);
        if (error || warning) console.warn(error || warning);
        const ploson = createRunner({ staticEnv });
        return { ...value, ploson };
      },
      transformUpdated: (item, { ploson, ref }) => ({ ...item, ploson, ref }),
    }),
    triggers: configuredCollectionDiff({
      ignoreKeys: ['ploson', 'callback', 'ref'],
      autoRemoveCondition: ({ active }) => !active,
      transformAutoRemoved: (item, { ref }) => ({ ...item, ref }),
      transformAdded: ({ targets = ARR, ...item }) => {
        const ploson = createRunner({ staticEnv: staticEnvTriggers });
        return { ...item, targets, ploson };
      },
      transformUpdated: (item, { ploson, callback, ref }, patch) => ({
        ...item,
        ploson,
        ref,
        ...(R.any(R.propEq('path', '/trigger'))(patch) || !item.active ? OBJ : { callback }),
      }),
    }),
  };

  const prepareAddedRule = createPrepareAddedRule(staticEnv);
  const prepareAddedTrigger = createPrepareAddedTrigger(staticEnvTriggers);

  return {
    delays: { RETRY_DELAY: 30_000 },

    services: {
      ruleFetcher,
      triggerFetcher,
      prepareAddedRule,
      prepareAddedTrigger,
      prepareUpdatedRule,
      prepareUpdatedTrigger,
    },

    actions: {
      blindRulesUpdate: pure(getBlindDatasetUpdateActions(diff, 'rule')),
      blindTriggersUpdate: pure(getBlindDatasetUpdateActions(diff, 'trigger')),
      defer: assign({
        $queue: ({ $queue }, e) => R.append(e, $queue),
      }),
      recallFromQueue: pure(({ $queue: [first, ...rest] }) => [
        ...(first ? [send(first)] : []),
        assign({ $queue: rest }),
      ]),
      addRuleExternal: assign({
        rules: ({ rules }, { data }) => R.append(getAddedRule(data))(rules),
      }),
      removeRule: pure(({ rules }, { data: { id } = OBJ }) => {
        const rIx = R.findIndex(R.propEq('id', id), rules);
        if (rIx === -1) return [];
        const { ref } = rules[rIx];
        return [stop(ref), assign({ rules: R.remove(rIx, 1)(rules) })];
      }),
      addTriggerExternal: assign({
        triggers: ({ triggers }, { data }) => R.append(getAddedTrigger(data))(triggers),
      }),
      removeTrigger: pure(({ triggers }, { data: { id } = OBJ }) => {
        const rIx = R.findIndex(R.propEq('id', id), triggers);
        if (rIx === -1) return [];
        const { ref } = triggers[rIx];
        return [stop(ref), assign({ triggers: R.remove(rIx, 1)(triggers) })];
      }),
      updateRuleExternal: pure(getUpdateRuleActions),
      updateTriggerExternal: pure(updateTriggerAction),
      logError: (ctx, { data }) => console.error(data),
      spawnUpdater: assign({ updaterRef: () => spawn(updater, 'rule-trigger-updater') }),
      fireRules: pure(({ rules }, { targets, payload }) => {
        // console.log('fireRules...:', targets, payload);
        const events = R.reduce(
          (acc, { id, active, ref }) =>
            active && ref && R.includes(id, targets)
              ? [...acc, send({ type: 'FIRE', payload }, { to: ref })]
              : acc,
          [],
        )(rules);
        // console.log('fireRules:', events);
        return events;
      }),
    },
  };
};

export const createRuleEngineMachine = (config) => {
  return createMachine(machineConf, getOptions(config));
};

export const createRuleEngine = (...args) => interpret(createRuleEngineMachine(...args));
