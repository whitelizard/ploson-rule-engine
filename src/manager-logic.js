import { spawn, send, assign, actions } from 'xstate';
import { createRunner, ARR } from 'ploson';
import * as R from 'ramda';
import isFunction from 'lodash.isfunction';
import { createPatch } from 'rfc6902';

import { ruleMachine } from './rule';
import { validateRule } from './rule-dm';
import { validateTrigger } from './trigger-dm';
import { runPlosonOrReject } from './processing';

export const getTriggerCallbackActor = (callback, targets) => (sendCb) =>
  callback((payload, dynamicTargets) => {
    // console.log('payload:', payload);
    return sendCb({ type: 'FIRE_RULES', targets: dynamicTargets ?? targets, payload });
  });

const { stop } = actions;

export const getBlindDatasetUpdateActions =
  (diff, dataset) =>
  ({ [`${dataset}s`]: items }, { data = ARR }) => {
    const { added, updated, removed } = diff[`${dataset}s`](items)(data);
    const entity = R.toUpper(dataset);
    return [
      ...R.map((item) => send({ type: `ADD_${entity}`, data: item }))(added),
      ...R.map((item) => send({ type: `UPDATE_${entity}`, data: item }))(updated),
      ...R.map((item) => send({ type: `DELETE_${entity}`, data: item }))(removed),
    ];
  };

/** RULE */

export const createPrepareAddedRule =
  (staticEnv) =>
  async (ctx, { data: item }) => {
    const [error, value] = validateRule(item);
    if (error) return Promise.reject(error);

    const ploson = createRunner({ staticEnv });
    const { onLoad } = item;
    if (onLoad) await runPlosonOrReject({ ploson, code: onLoad });
    // console.log('RULE TO ADD:', value);
    return { ...value, ploson };
  };

export const prepareUpdatedRule = async ({ rules }, { data: item }) => {
  const [error, value] = validateRule(item);
  if (error) return Promise.reject(error);

  const prevItem = R.find(R.propEq('id', value.id))(rules);
  const { ref, ploson, ...pureItem } = prevItem;
  // console.log('prepareUpdatedRule items:', pureItem, value);
  const patch = createPatch(pureItem, value);
  // console.log('prepareUpdatedRule patch:', patch);
  return { ...value, ploson, ref, lastPatch: patch };
};

export const getUpdateRuleActions = ({ rules }, { data: rule }) => {
  const { id, lastPatch, ref, active } = rule;
  const ix = R.findIndex(R.propEq('id', id), rules);
  const conditionChange = R.any(R.pipe(R.prop('path'), R.startsWith('/condition')))(lastPatch);
  const activated = R.pipe(R.find(R.propEq('path', '/active')), R.propEq('value', true))(lastPatch);
  const actionObjects = [
    assign({ rules: R.update(ix, rule)(rules) }),
    send({ type: 'UPDATE', rule }, { to: ref }),
    ...(!active ? [send('INACTIVATE', { to: ref })] : []),
    ...(conditionChange ? [send('ABORT', { to: ref })] : []),
    ...(activated ? [send('ACTIVATE', { to: ref })] : []),
  ];
  return actionObjects;
};

export const getAddedRule = ({ id, active, ...rest }) => {
  console.info('Spawning Rule with id:', id);
  const spawnedIfActive = active ? { ref: spawn(ruleMachine.withContext(rest), id) } : {};
  return { id, active, ...rest, ...spawnedIfActive };
};

/** TRIGGER */

export const createPrepareAddedTrigger =
  (staticEnvTriggers) =>
  async (ctx, { data }) => {
    const [error, value] = validateTrigger(data);
    if (error) return Promise.reject(error);

    const { trigger, ...item } = value;
    const ploson = createRunner({ staticEnv: staticEnvTriggers });
    const callback = await runPlosonOrReject({ ploson, code: trigger });
    if (!isFunction(callback)) return Promise.reject(Error('trigger is not a function'));
    return { ...item, trigger, ploson, callback };
  };

export const prepareUpdatedTrigger = async ({ triggers }, { data }) => {
  const [error, value] = validateTrigger(data);
  if (error) return Promise.reject(error);
  const { id, trigger, active } = value;

  const prevItem = R.find(R.propEq('id', id))(triggers);
  const { callback, ploson, ref, doUpdateTrigger: _, ...pureItem } = prevItem;
  const patch = createPatch(pureItem, value);

  const activated = R.pipe(R.find(R.propEq('path', '/active')), R.propEq('value', true))(patch);
  const doUpdateTrigger = activated || (active && R.any(R.propEq('path', '/trigger'))(patch));

  const newCallback = doUpdateTrigger && (await runPlosonOrReject({ ploson, code: trigger }));
  if (newCallback && !isFunction(newCallback))
    return Promise.reject(Error('trigger is not a function'));
  return {
    ...value,
    ploson,
    callback,
    ref,
    ...(doUpdateTrigger ? { callback: newCallback } : {}),
    doUpdateTrigger,
  };
};

export const getAddedTrigger = ({ active, callback, targets, id, ...rest }) => {
  console.info('Spawning Trigger with id:', id);
  const spawnedIfActive = active
    ? { ref: spawn(getTriggerCallbackActor(callback, targets), id) }
    : {};
  return { id, active, targets, callback, ...rest, ...spawnedIfActive };
};

export const updateTriggerAction = ({ triggers }, { data }) => {
  const { id, doUpdateTrigger, callback, ref, targets, active } = data;
  const ix = R.findIndex(R.propEq('id', id), triggers);

  const actionObjects = [
    ...(doUpdateTrigger || (!active && ref) ? [stop(ref)] : []),
    ...(doUpdateTrigger ? [spawn(getTriggerCallbackActor(callback, targets), id)] : []),
    assign({ triggers: R.update(ix, data)(triggers) }),
  ];
  // console.log('actionObjects:', actionObjects);
  return actionObjects;
};
