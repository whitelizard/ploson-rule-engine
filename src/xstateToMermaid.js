import { assign, createMachine } from 'xstate';

/**
 * Strategy
 *
 * Use JSON of the machine confid to avoid normalizations createMachine does
 *
 * - Guards
 * - Invocations
 * - timers
 * -
 *
 */

const machine = createMachine({
  predictableActionArguments: true,
  id: 'rule',
  initial: 'idle',
  on: {
    INACTIVATE: 'inactive',
    UPDATE: { actions: 'updateRule' },
  },
  states: {
    inactive: { on: { ACTIVATE: 'idle' } },
    idle: {
      on: {
        FIRE: [{ cond: 'isExpired', target: 'expired' }, { actions: 'passOn' }],
        PASS_ON: 'checkCondition',
      },
    },
    checkCondition: {
      invoke: {
        src: 'condition',
        onDone: [{ cond: 'conditionTruthy', target: 'executeAction' }, { target: 'idle' }],
        onError: { actions: 'logError', target: 'idle' },
      },
    },
    executeAction: {
      invoke: {
        src: 'action',
        onDone: { target: 'fired' },
        onError: { actions: 'logError', target: 'fired' },
      },
    },
    fired: {
      type: 'parallel',
      on: { ABORT: 'idle' },
      states: {
        switcher: {
          initial: 'decide',
          states: {
            decide: {
              always: [{ cond: 'hasHysteresis', target: 'flipped' }, { target: 'done' }],
            },
            flipped: { on: { FIRE: 'checkCondition' } },
            checkCondition: {
              invoke: {
                src: 'resetCondition',
                onDone: [
                  { cond: 'conditionTruthy', target: 'executeResetAction' },
                  { target: 'flipped' },
                ],
                onError: { actions: 'logError', target: 'flipped' },
              },
            },
            executeResetAction: {
              invoke: {
                src: 'resetAction',
                onDone: { target: 'done' },
                onError: { actions: 'logError', target: 'done' },
              },
            },
            done: { type: 'final' },
          },
        },
        temperature: {
          initial: 'decide',
          states: {
            decide: {
              always: [{ cond: 'hasCooldown', target: 'hot' }, { target: 'cool' }],
            },
            hot: { after: { COOLDOWN: 'cool' } },
            cool: { type: 'final' },
          },
        },
      },
      onDone: 'idle',
    },
    expired: { type: 'final' },
  },
});

// console.log("result: ", xstateToMermaid(machine));

const getActions = (actions) => {
  if (!actions) return [];
  if (!Array.isArray(actions)) return getActions([actions]);

  return actions
    .map((action) => {
      if (typeof action === 'function') return 'anonymous';
      if (typeof action === 'string') return action;

      // TODO: inline actions objects (from action creators) and parametrized actions

      return undefined;
    })
    .filter(Boolean);
};

const getCond = (cond) => (!cond ? undefined : typeof cond === 'function' ? 'anonymous' : cond);

const getTransitions = (transitions) => {
  if (!transitions) return [];
  if (typeof transitions === 'string') return getTransitions([{ target: transitions }]);
  if (!Array.isArray(transitions)) return getTransitions([transitions]);

  const condInUse = transitions.some(({ cond }) => Boolean(cond));
  return transitions.map(({ target, cond, actions }) => ({
    condInUse,
    // TODO: internal/external? in?
    cond: getCond(cond),
    target,
    actions: getActions(actions),
  }));
};

const visit = (state, ctx) => {
  const finalId = state.id || ctx.path.join('.');

  const normalized = {
    id: finalId,
    key: ctx.path[ctx.path.length - 1],
    type: state.type || (state.states ? 'compound' : 'atomic'),
    parent: ctx.parent,
    entry: getActions(state.entry),
    exit: getActions(state.exit),
    // TODO: invokes
    invoke: Object.fromEntries(
      Object.entries(
        state.invoke ? { onDone: state.invoke.onDone, onError: state.invoke.onError } : {},
      ).map(([eventName, transition]) => {
        return [eventName, getTransitions(transition)];
      }),
    ),
    initial: state.initial,
    states:
      state.states &&
      Object.fromEntries(
        Object.entries(state.states).map(([subStateKey, subState]) => {
          return [
            subStateKey,
            visit(subState, {
              ...ctx,
              path: [...ctx.path, subStateKey],
              parent: finalId,
            }),
          ];
        }),
      ),
    // TODO:
    always: getTransitions(state.always),
    after: Object.fromEntries(
      Object.entries(state.after || {}).map(([delayName, transition]) => [
        delayName,
        getTransitions(transition),
      ]),
    ),
    on: Object.fromEntries(
      Object.entries(state.on || {}).map(([eventName, transition]) => [
        eventName,
        getTransitions(transition),
      ]),
    ),
  };
  ctx.idMap.set(normalized.id, normalized);
  return normalized;
};

const renderActions = (prefix, actions) =>
  !actions.length ? '' : `    ${prefix}: ${actions.join(', ')}`;

const renderHeader = (state) =>
  `state "${[
    !state.parent ? state.id : state.key,
    renderActions('entry', state.entry),
    renderActions('exit', state.exit),
  ]
    .filter(Boolean)
    .join('\n')}" as ${encodeURIComponent(state.id)}`;

const renderTransition =
  (ctx, { parent, id }, eventName = 'always') =>
  ({ target, cond, condInUse }) => {
    // TODO: forbidden transitions
    if (!target) return '';
    const name = target.startsWith('#') ? target.slice(1) : ctx.idMap.get(parent).states[target].id;

    return `${encodeURIComponent(id)} --> ${encodeURIComponent(name)}: ${eventName} ${
      cond ? `[${cond}]` : condInUse ? '[default]' : ''
    }`;
  };

const render = (state, ctx) => {
  const header = Object.values(state.states).map(renderHeader);

  const always = Object.values(state.states || {}).flatMap(
    (subState) => subState.always && subState.always.map(renderTransition(ctx, subState)),
  );
  const transitions = Object.values(state.states || {}).flatMap((subState) =>
    Object.entries(subState.on).flatMap(([eventName, trans]) =>
      trans.map(renderTransition(ctx, subState, eventName)),
    ),
  );
  const afters = Object.values(state.states || {}).flatMap((subState) =>
    Object.entries(subState.after).flatMap(([eventName, trans]) =>
      trans.map(renderTransition(ctx, subState, eventName)),
    ),
  );
  const invokes = Object.values(state.states || {}).flatMap((subState) =>
    Object.entries(subState.invoke).flatMap(([eventName, trans]) => {
      if (['src', 'id'].includes(eventName)) return '';
      return trans.map(renderTransition(ctx, subState, eventName));
    }),
  );

  const stringifiedStates = Object.entries(state.states)
    .flatMap(([, subState]) =>
      ['compound', 'parallel'].includes(subState.type)
        ? [`state ${encodeURIComponent(subState.id)} {\n${render(subState, ctx)}\n}`]
        : [],
    )
    .join(state.type === 'parallel' ? '\n--\n' : '\n\n');

  return [
    ...header,
    ...(state.initial ? [`[*] --> ${encodeURIComponent(state.states[state.initial].id)}`] : []),
    ...stringifiedStates.split('\n'),
    ...always,
    ...transitions,
    ...invokes,
    ...afters,
  ]
    .map((line) => `  ${line}`)
    .join('\n')
    .trimEnd();
};

const xstateToMermaid2 = ({ config }) => {
  const ctx = {
    idMap: new Map(),
  };
  const visited = visit(config, { ...ctx, path: ['machine'], parent: undefined });

  const body = [
    renderHeader(visited),
    `state ${encodeURIComponent(visited.id)} {\n${render(visited, ctx)}\n}`,
  ]
    .join('\n')
    .split('\n')
    .map((line) => `  ${line}`)
    .join('\n');

  return `stateDiagram-v2\n${body}`;
};

console.log(xstateToMermaid2(machine));
