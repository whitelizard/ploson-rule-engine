import * as R from 'ramda';
import { createPatch } from 'rfc6902';
import { OBJ } from 'ploson';
import isFunction from 'lodash.isfunction';

export const idUniquesInFirst = R.differenceWith(
  ({ id: id1 } = OBJ, { id: id2 } = OBJ) => id1 === id2,
);

export const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

export const createInterval = (func, ms, val) => {
  if (!isFunction(func)) return undefined;
  const id = setInterval(func, ms, val);
  return () => clearInterval(id);
};

export const createTimeout = (func, ms, val) => {
  if (!isFunction(func)) return undefined;
  const id = setTimeout(func, ms, val);
  return () => clearTimeout(id);
};

export const configuredCollectionDiff =
  ({
    idKey = 'id',
    ignoreKeys,
    skipIf,
    autoRemoveCondition,
    transformAutoRemoved,
    transformAdded,
    transformUpdated,
  } = {}) =>
  (prev) =>
    R.converge(R.mergeLeft, [
      R.reduce(
        (acc, item) => {
          if (skipIf && skipIf(item)) return acc;
          if (autoRemoveCondition && autoRemoveCondition(item)) return acc;
          const { [idKey]: id } = item;
          const prevItem = R.find(R.propEq('id', id))(prev);
          if (prevItem) {
            const args = R.when(() => ignoreKeys, R.map(R.omit(ignoreKeys)))([prevItem, item]);
            const patch = createPatch(...args);
            return R.isEmpty(patch)
              ? acc
              : {
                  ...acc,
                  updated: [
                    ...acc.updated,
                    transformUpdated ? transformUpdated(item, prevItem, patch) : item,
                  ],
                };
          }
          return { ...acc, added: [...acc.added, transformAdded ? transformAdded(item) : item] };
        },
        { added: [], updated: [] },
      ),
      R.pipe(
        R.converge(R.concat, [
          idUniquesInFirst(prev),
          R.pipe(
            R.filter(autoRemoveCondition ?? R.F),
            R.map((item) => {
              const { [idKey]: id } = item;
              const prevItem = R.find(R.propEq('id', id))(prev);
              return transformAutoRemoved ? transformAutoRemoved(item, prevItem) : item;
            }),
          ),
        ]),
        R.objOf('removed'),
      ),
    ]);

export const collectionDiff = configuredCollectionDiff();
