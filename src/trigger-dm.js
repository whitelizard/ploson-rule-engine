import joi from 'joi';

// .required()
// actuator: joi
//   .string()
//   .default('backend')
//   .description('ID of where the rule should be loaded and run.'),

export const Trigger = joi
  .object()
  .unknown()
  .keys({
    id: joi.string().required().description('Identifier for this particular trigger.'),
    active: joi
      .boolean()
      .default(true)
      .description('If the trigger is active or not. An inactive trigger is not run at all.'),
    trigger: joi
      .array()
      .required()
      .items(joi.any())
      .description('Ploson function representing a callback actor.'),
    // }),
    targets: joi
      .array()
      .items(joi.string())
      .unique()
      .description('List of rule IDs that this trigger should trigger.'),
  });

export const validateTrigger = (item) => {
  const { value, error, warning } = Trigger.validate(item);
  if (warning) console.warn(warning);
  return error ? [Error(error)] : [undefined, value];
};
