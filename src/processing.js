import * as R from 'ramda';
import isError from 'lodash.iserror';

export const runPlosonOrReject = async ({ ploson, code, createReturn = R.identity }) => {
  const result = await ploson(code);
  return isError(result) ? Promise.reject(result) : createReturn(result);
};
