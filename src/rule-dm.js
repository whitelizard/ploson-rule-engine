import joi from 'joi';

// .required()
// actuator: joi
//   .string()
//   .default('backend')
//   .description('ID of where the rule should be loaded and run.'),

export const Rule = joi
  .object()
  .unknown()
  .keys({
    id: joi.string().required().description('Identifier for this particular rule.'),
    active: joi
      .boolean()
      .default(true)
      .description('If the rule is active or not. An inactive rule is not run at all.'),
    ttl: joi
      .date()
      .default(17407501722694)
      .description('At this time (ISO timestamp) the rule will be set to inactive.'),
    onLoad: joi.any().description('Ploson code to e.g. create reusable constants and functions.'),
    // triggered: joi.object().keys({
    cooldown: joi
      .number()
      .min(0)
      .description("A rule can't be triggered again unless this number of seconds has passed."),
    resetCondition: joi
      .any()
      .description('Ploson code to check if rule should reset, if it is in flipped state.'),
    resetAction: joi.any().description('Ploson code block to execute when resetCondition is true.'),
    // }),
    condition: joi
      .any()
      .default(true)
      .description(
        'Ploson code to check if rule should execute (state to flipped, run actions etc).',
      ),
    action: joi
      .any()
      .description('Ploson code block to execute when condition is true (& not in flipped state).'),
  });

export const validateRule = (item) => {
  const { value, error, warning } = Rule.validate(item);
  if (warning) console.warn(warning);
  return error ? [Error(error)] : [undefined, value];
};
